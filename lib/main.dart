import 'package:farmatodo_register_app/generated/l10n.dart';
import 'package:farmatodo_register_app/src/config/colors/themes/color_schemes.dart';
import 'package:farmatodo_register_app/src/config/router/app_router.dart';
import 'package:farmatodo_register_app/src/domain/repositories/database_repository.dart';
import 'package:farmatodo_register_app/src/locator.dart';
import 'package:farmatodo_register_app/src/presentation/blocs/employees_bloc/employees_bloc.dart';
import 'package:farmatodo_register_app/src/utils/constants/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'src/utils/observers/app_bloc_observer.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initializeDependencies();

  Bloc.observer = AppBlocObserver();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => EmployeesBloc(locator.get<DatabaseRepository>())
            ..add(HomeLoadAllEmployeesEvent()),
        )
      ],
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(colorScheme: lightColorScheme),
        routerConfig: appRouter.config(),
        title: appTitle,
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
      ),
    );
  }
}
