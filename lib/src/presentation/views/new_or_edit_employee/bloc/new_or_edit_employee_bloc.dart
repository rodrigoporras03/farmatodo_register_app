import 'package:bloc/bloc.dart';

import 'package:meta/meta.dart';

part 'new_or_edit_employee_event.dart';
part 'new_or_edit_employee_state.dart';

class NewOrEditEmployeeBloc
    extends Bloc<NewOrEditEmployeeEvent, NewOrEditEmployeeState> {
  NewOrEditEmployeeBloc() : super(NewOrEditEmployeeInitial()) {
    on<UpdateAdmisionDateFieldEvent>(
        _updateRegistrationDateFieldEventToState);
  }

  _updateRegistrationDateFieldEventToState(
    UpdateAdmisionDateFieldEvent event,
    Emitter<NewOrEditEmployeeState> emit,
  ) {
    emit(UpdateRegistrationDateState(event.newRegistrationDate));
  }
}
