part of 'new_or_edit_employee_bloc.dart';

@immutable
abstract class NewOrEditEmployeeState {
  final DateTime? admisionDate;

  const NewOrEditEmployeeState([this.admisionDate]);
}

class NewOrEditEmployeeInitial extends NewOrEditEmployeeState {}

class UpdateRegistrationDateState extends NewOrEditEmployeeState {
  final DateTime newRegistrationDate;

  const UpdateRegistrationDateState(this.newRegistrationDate)
      : super(newRegistrationDate);
}
