part of 'new_or_edit_employee_bloc.dart';

@immutable
abstract class NewOrEditEmployeeEvent {}

class UpdateAdmisionDateFieldEvent extends NewOrEditEmployeeEvent {
  final DateTime newRegistrationDate;

  UpdateAdmisionDateFieldEvent(this.newRegistrationDate);
}


