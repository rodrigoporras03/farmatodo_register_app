import 'package:auto_route/auto_route.dart';
import 'package:farmatodo_register_app/src/config/colors/themes/color_schemes.dart';
import 'package:farmatodo_register_app/src/config/config.dart';
import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/presentation/blocs/employees_bloc/employees_bloc.dart';
import 'package:farmatodo_register_app/src/presentation/views/new_or_edit_employee/bloc/new_or_edit_employee_bloc.dart';
import 'package:farmatodo_register_app/src/presentation/widgets/app_dropdown_button_form_field.dart';
import 'package:farmatodo_register_app/src/presentation/widgets/app_text_form_field.dart';
import 'package:farmatodo_register_app/src/utils/constants/app_default_insets.dart';
import 'package:farmatodo_register_app/src/utils/dialogs/default_dialog.dart';
import 'package:farmatodo_register_app/src/utils/validators/validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

@RoutePage()
class NewOrEditEmployeeView extends StatefulWidget {
  final Employee? employeeForEdit;
  const NewOrEditEmployeeView({super.key, this.employeeForEdit});

  @override
  State<NewOrEditEmployeeView> createState() => _NewOrEditEmployeeViewState();
}

class _NewOrEditEmployeeViewState extends State<NewOrEditEmployeeView> {
  final bloc = NewOrEditEmployeeBloc();

  final formKey = GlobalKey<FormState>();

  //InputController Area
  final surnameController = TextEditingController();

  final lastNameController = TextEditingController();

  final nameController = TextEditingController();

  final middleNameController = TextEditingController();

  final idNumberController = TextEditingController();

  CountryEnum? countrySelection;
  IdTypeEnum? idTypeSelection;
  AreaEnum? areaSelection;
  final StatusEnum statusSelection = StatusEnum.active;

  @override
  void initState() {
    if (widget.employeeForEdit != null) {
      surnameController.text = widget.employeeForEdit!.surname;
      lastNameController.text = widget.employeeForEdit!.lastName;
      nameController.text = widget.employeeForEdit!.name;
      middleNameController.text = widget.employeeForEdit!.middleName;
      idNumberController.text = widget.employeeForEdit!.idNumber;

      countrySelection = widget.employeeForEdit!.country;
      idTypeSelection = widget.employeeForEdit!.idType;
      areaSelection = widget.employeeForEdit!.area;

      bloc.add(
          UpdateAdmisionDateFieldEvent(widget.employeeForEdit!.admisionDate));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EmployeesBloc, EmployeesState>(
      listener: (context, state) {
        if (state is NewOrEditEmployeeErrorState) {
          showDefaultDialog(
            context: context,
            title: 'Error',
            content: Text(state.errorMessage),
            onClose: () async {},
          );
        } else if (state is EmployeCreatedState ||
            state is EmployeeEditedState) {
          showDefaultDialog(
            context: context,
            title: state is EmployeCreatedState
                ? '¡Empleado Creado!'
                : '¡Empleado Actualizado!',
            content: const SizedBox.shrink(),
            onClose: context.router.pop,
          );
        }
      },
      child: BlocBuilder<NewOrEditEmployeeBloc, NewOrEditEmployeeState>(
        bloc: bloc,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.employeeForEdit != null
                  ? 'Editar Empleado'
                  : labels.newEmployeeTitle),
            ),
            body: Padding(
              padding: const EdgeInsets.all(AppDefaultInsets.s16),
              child: Column(
                children: [
                  _buildForm(context, state),
                  MaterialButton(
                    minWidth: double.infinity,
                    onPressed: () {
                      if (!formKey.currentState!.validate() ||
                          state.admisionDate == null) {
                        return;
                      }

                      widget.employeeForEdit != null
                          ? context.read<EmployeesBloc>().add(
                                UpdateEmployeeEvent(
                                  employeeToUpdate:
                                      widget.employeeForEdit!.copyWith(
                                    name: nameController.text.trim(),
                                    middleName:
                                        middleNameController.text.trim(),
                                    surname: surnameController.text.trim(),
                                    lastName: lastNameController.text.trim(),
                                    idType: idTypeSelection!,
                                    idNumber: idNumberController.text.trim(),
                                    admisionDate: state.admisionDate!,
                                    area: areaSelection!,
                                    country: countrySelection!,
                                    status: statusSelection,
                                  ),
                                ),
                              )
                          : context.read<EmployeesBloc>().add(
                                CreateEmployeeEvent(
                                  name: nameController.text,
                                  middleName: middleNameController.text,
                                  surname: surnameController.text,
                                  lastName: lastNameController.text,
                                  idType: idTypeSelection!,
                                  idNumber: idNumberController.text,
                                  admisionDate: state.admisionDate!,
                                  area: areaSelection!,
                                  country: countrySelection!,
                                  status: statusSelection,
                                ),
                              );
                    },
                    color: lightColorScheme.primary,
                    child: Text(
                      widget.employeeForEdit != null ? 'Guardar' : 'Crear',
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Flexible _buildForm(BuildContext context, NewOrEditEmployeeState state) {
    return Flexible(
      child: Form(
        key: formKey,
        child: ListView(
          children: [
            _buildInputTitle(labels.surname),
            AppTextFormField(
              controller: surnameController,
              validator: Validators.validateSurnnames,
              automaticUpperCase: true,
              maxLength: 20,
            ),
            _buildInputTitle(labels.lastName),
            AppTextFormField(
              controller: lastNameController,
              validator: Validators.validateSurnnames,
              automaticUpperCase: true,
              maxLength: 20,
            ),
            _buildInputTitle(labels.name),
            AppTextFormField(
              controller: nameController,
              validator: Validators.validateNames,
              automaticUpperCase: true,
              maxLength: 20,
            ),
            _buildInputTitle(labels.middleName),
            AppTextFormField(
              controller: middleNameController,
              validator: Validators.validateNames,
              automaticUpperCase: true,
              maxLength: 50,
            ),
            _buildInputTitle(labels.country),
            AppDropdownButtonFormField(
              value: widget.employeeForEdit?.country,
              hint: labels.country,
              items: CountryEnum.values,
              onChanged: (p0) => countrySelection = p0,
            ),
            const SizedBox(height: AppDefaultInsets.s8),
            _buildInputTitle(labels.idType),
            AppDropdownButtonFormField<IdTypeEnum>(
              value: widget.employeeForEdit?.idType,
              hint: labels.idType,
              items: IdTypeEnum.values,
              onChanged: (p0) => idTypeSelection = p0,
            ),
            const SizedBox(height: AppDefaultInsets.s8),
            _buildInputTitle(labels.idNumber),
            AppTextFormField(
              controller: idNumberController,
              hintText: labels.idNumber,
              validator: Validators.validateIdNumber,
              maxLength: 20,
            ),
            const SizedBox(height: AppDefaultInsets.s8),
            _buildInputTitle(labels.registerDate),
            InkWell(
              onTap: () async {
                final newRegistrationDate = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now().subtract(const Duration(days: 31)),
                  lastDate: DateTime.now(),
                  builder: (context, child) {
                    return child!;
                  },
                );

                if (newRegistrationDate != null) {
                  bloc.add(UpdateAdmisionDateFieldEvent(newRegistrationDate));
                }
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(AppDefaultInsets.s18),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(color: Colors.grey[500]!),
                    ),
                    child: state.admisionDate != null
                        ? Text(DateFormat('dd/MM/yyyy')
                            .format(state.admisionDate!))
                        : const Text('Seleccionar...'),
                  ),
                  state.admisionDate == null
                      ? Padding(
                          padding: const EdgeInsets.only(
                              top: AppDefaultInsets.s8,
                              left: AppDefaultInsets.s16),
                          child: Text(
                            'Por favor ingrese este campo',
                            style: TextStyle(
                                fontSize: 12, color: lightColorScheme.error),
                          ),
                        )
                      : const SizedBox.shrink(),
                ],
              ),
            ),
            const SizedBox(height: AppDefaultInsets.s8),
            _buildInputTitle(labels.area),
            AppDropdownButtonFormField(
              value: widget.employeeForEdit?.area,
              hint: labels.area,
              items: AreaEnum.values,
              onChanged: (p0) => areaSelection = p0,
            ),
            const SizedBox(height: AppDefaultInsets.s8),
            _buildInputTitle('Estado'),
            const AppDropdownButtonFormField(
              value: StatusEnum.active,
              items: StatusEnum.values,
            ),
            const SizedBox(
              height: AppDefaultInsets.s18,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInputTitle(String title) {
    return Padding(
      padding: const EdgeInsets.only(bottom: AppDefaultInsets.s8),
      child: Text(title),
    );
  }
}
