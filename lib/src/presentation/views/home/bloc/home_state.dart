part of 'home_bloc.dart';

@immutable
abstract class HomeState {
  final List<Filter> filters;

  const HomeState([this.filters = const []]);
}

class HomeInitial extends HomeState {}

class HomeShowFilteredList extends HomeState {
  final List<Employee> filteredEmployeeList;

  const HomeShowFilteredList(super.filters, this.filteredEmployeeList);
}
