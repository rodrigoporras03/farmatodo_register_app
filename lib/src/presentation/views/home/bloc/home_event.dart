part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class HomeAddFilterEvent extends HomeEvent {
  final List<Employee> employeesToFilter;
  final Filter filter;

  HomeAddFilterEvent(this.employeesToFilter, this.filter);
}

class HomeRemoveAllFiltersEvent extends HomeEvent {
  HomeRemoveAllFiltersEvent();
}
