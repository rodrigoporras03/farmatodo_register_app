import 'package:bloc/bloc.dart';
import 'package:farmatodo_register_app/src/domain/enums/filter_enum.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/domain/models/filter.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<HomeAddFilterEvent>(_homeFilterEventToState);
    on<HomeRemoveAllFiltersEvent>(_homeRemoveAllFiltersEvent);
  }

  _homeFilterEventToState(
    HomeAddFilterEvent event,
    Emitter emit,
  ) {
    final finalFilters = [...state.filters, event.filter];

    List<Employee> employeedFiltered = event.employeesToFilter;

    for (Filter filter in finalFilters) {
      switch (filter.filterEnum) {
        case FilterEnum.name:
          employeedFiltered = employeedFiltered
              .where((e) => e.name.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.middleName:
          employeedFiltered = employeedFiltered
              .where((e) => e.middleName.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.surname:
          employeedFiltered = employeedFiltered
              .where((e) => e.surname.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.lastName:
          employeedFiltered = employeedFiltered
              .where((e) => e.lastName.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.email:
          employeedFiltered = employeedFiltered
              .where((e) => e.email.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.idNumber:
          employeedFiltered = employeedFiltered
              .where((e) => e.idNumber.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.country:
          employeedFiltered = employeedFiltered
              .where((e) => e.country.tr.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.idType:
          employeedFiltered = employeedFiltered
              .where((e) => e.idType.tr.toLowerCase().contains(filter.query))
              .toList();
          break;
        case FilterEnum.status:
          employeedFiltered = employeedFiltered
              .where((e) => e.status.tr.toLowerCase().contains(filter.query))
              .toList();
          break;
      }
    }

    emit(HomeShowFilteredList(
      finalFilters,
      employeedFiltered,
    ));
  }

  _homeRemoveAllFiltersEvent(
    HomeRemoveAllFiltersEvent event,
    Emitter emit,
  ) {
    emit(HomeInitial());
  }
}
