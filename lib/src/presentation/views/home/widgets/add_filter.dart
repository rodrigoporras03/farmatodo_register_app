import 'package:auto_route/auto_route.dart';
import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:farmatodo_register_app/src/domain/enums/filter_enum.dart';
import 'package:farmatodo_register_app/src/domain/models/filter.dart';
import 'package:farmatodo_register_app/src/presentation/widgets/app_dropdown_button_form_field.dart';
import 'package:farmatodo_register_app/src/presentation/widgets/app_text_form_field.dart';
import 'package:farmatodo_register_app/src/utils/constants/app_default_insets.dart';
import 'package:flutter/material.dart';

class AddFilter extends StatefulWidget {
  const AddFilter({super.key});

  @override
  State<AddFilter> createState() => _AddFilterState();
}

class _AddFilterState extends State<AddFilter> {
  FilterEnum? selectedFilter;
  final fieldTextController = TextEditingController();
  String? query;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppDropdownButtonFormField(
          hint: 'Selecciona un filtro',
          items: FilterEnum.values,
          onChanged: (p0) {
            selectedFilter = p0;
            setState(() {});
          },
        ),
        const SizedBox(height: AppDefaultInsets.s16),
        if (selectedFilter != null) _buildFilterField(selectedFilter!),
        TextButton(
          onPressed: () {
            if (query != null && selectedFilter != null) {
              context.router.pop(Filter(
                filterEnum: selectedFilter!,
                query: query!.toLowerCase(),
              ));
            } else {
              context.router.pop();
            }
          },
          child: const Text('Aplicar Filtros'),
        ),
      ],
    );
  }

  _buildFilterField(FilterEnum filter) {
    switch (filter) {
      case FilterEnum.country:
        return AppDropdownButtonFormField(
          hint: 'Seleccione',
          items: CountryEnum.values,
          onChanged: (p0) => query = p0?.tr ?? '',
        );
      case FilterEnum.idType:
        return AppDropdownButtonFormField(
          hint: 'Seleccione',
          items: IdTypeEnum.values,
          onChanged: (p0) => query = p0?.tr ?? '',
        );
      case FilterEnum.status:
        return AppDropdownButtonFormField(
          hint: 'Seleccione',
          items: StatusEnum.values,
          onChanged: (p0) => query = p0?.tr ?? '',
        );
      default:
        return AppTextFormField(
          controller: fieldTextController,
          hintText: selectedFilter!.tr,
          onChanged: (p0) {
            query = p0;
          },
        );
    }
  }
}
