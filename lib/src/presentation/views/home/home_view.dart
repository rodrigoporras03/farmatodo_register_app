import 'package:auto_route/auto_route.dart';
import 'package:farmatodo_register_app/src/config/colors/themes/color_schemes.dart';
import 'package:farmatodo_register_app/src/config/config.dart';
import 'package:farmatodo_register_app/src/config/router/app_router.dart';
import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/presentation/blocs/employees_bloc/employees_bloc.dart';
import 'package:farmatodo_register_app/src/presentation/views/home/bloc/home_bloc.dart';
import 'package:farmatodo_register_app/src/presentation/views/home/widgets/add_filter.dart';
import 'package:farmatodo_register_app/src/utils/constants/app_default_insets.dart';
import 'package:farmatodo_register_app/src/utils/dialogs/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

@RoutePage()
class HomeView extends StatelessWidget {
  HomeView({super.key});

  final homeBloc = HomeBloc();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EmployeesBloc, EmployeesState>(
      builder: (context, employeeState) {
        return BlocBuilder<HomeBloc, HomeState>(
          bloc: homeBloc,
          builder: (context, homeState) {
            return Scaffold(
              appBar: AppBar(
                title: Text(labels.homeTitle),
                actions: [
                  IconButton(
                    onPressed: () async {
                      final newFilter = await showDefaultDialog(
                        title: 'Agregar Filtro',
                        context: context,
                        content: const AddFilter(),
                        showOkbtn: false,
                      );

                      if (newFilter != null) {
                        homeBloc.add(
                          HomeAddFilterEvent(
                            employeeState.allEmployees,
                            newFilter,
                          ),
                        );
                      }
                    },
                    icon: const Icon(Icons.filter_alt),
                  )
                ],
              ),
              body: Column(
                children: [
                  if (homeState.filters.isNotEmpty) ...[
                    const Padding(
                      padding: EdgeInsets.all(AppDefaultInsets.s8),
                      child: Text('Filtros Aplicados'),
                    ),
                    Wrap(
                      runSpacing: -10,
                      spacing: 5,
                      children: homeState.filters
                          .map((f) => FilterChip(
                              label: Text('${f.filterEnum.tr}: ${f.query}'),
                              onSelected: (_) {}))
                          .toList(),
                    ),
                    InkWell(
                      onTap: () {
                        homeBloc.add(HomeRemoveAllFiltersEvent());
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Borrar Filtros',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: lightColorScheme.primary),
                        ),
                      ),
                    ),
                    const Divider()
                  ],
                  employeeState.allEmployees.isNotEmpty
                      ? Expanded(
                          child: ListView.builder(
                            itemCount: homeState is HomeShowFilteredList
                                ? homeState.filteredEmployeeList.length
                                : employeeState.allEmployees.length,
                            itemBuilder: (BuildContext context, int index) {
                              return _buildEmployeeCard(
                                context,
                                homeState is HomeShowFilteredList
                                    ? homeState.filteredEmployeeList[index]
                                    : employeeState.allEmployees[index],
                              );
                            },
                          ),
                        )
                      : const Expanded(
                          child: Center(
                              child: Text('No hay empleados Registrados')))
                ],
              ),
              floatingActionButton: FloatingActionButton(
                child: const Icon(Icons.add),
                onPressed: () => context.router.push(NewOrEditEmployeeRoute()),
              ),
            );
          },
        );
      },
    );
  }

  Widget _buildEmployeeCard(BuildContext context, Employee employee) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(AppDefaultInsets.s8),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(employee.getFullName()),
                const SizedBox(height: AppDefaultInsets.s2),
                Text(employee.email.toLowerCase()),
                const SizedBox(height: AppDefaultInsets.s2),
                Row(
                  children: [
                    Text(employee.idType.tr),
                    const SizedBox(width: AppDefaultInsets.s8),
                    Text(employee.idNumber),
                  ],
                ),
                Row(
                  children: [
                    Text(employee.country.tr),
                    const SizedBox(width: AppDefaultInsets.s8),
                    Text(employee.status.tr),
                    const SizedBox(width: AppDefaultInsets.s2),
                    CircleAvatar(
                      radius: 5,
                      backgroundColor: employee.status == StatusEnum.active
                          ? Colors.green
                          : Colors.red,
                    ),
                    const SizedBox(width: AppDefaultInsets.s8),
                  ],
                ),
                Row(
                  children: [
                    const Text('Fecha de Ingreso:'),
                    const SizedBox(width: AppDefaultInsets.s8),
                    Text(
                        DateFormat('yyyy-MM-dd').format(employee.admisionDate)),
                  ],
                ),
                Row(
                  children: [
                    const Text('Ultima Actualización:'),
                    const SizedBox(width: AppDefaultInsets.s8),
                    Text(
                        DateFormat('yyyy-MM-dd').format(employee.lastUpdateDate)),
                  ],
                ),
              ],
            ),
            const Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                    onPressed: () {
                      context.router.push(
                          NewOrEditEmployeeRoute(employeeForEdit: employee));
                    },
                    icon: const Icon(Icons.edit)),
                IconButton(
                    onPressed: () {
                      showDefaultDialog(
                        context: context,
                        showOkbtn: false,
                        title: 'Borrar Empleado',
                        labelYes: 'Si',
                        labelNo: 'No',
                        content: Column(
                          children: [
                            const Text(
                                '¿Está seguro de que desea eliminar el empleado?'),
                            Text(employee.getFullName()),
                            Text(employee.idType.tr),
                            Text(employee.idNumber),
                          ],
                        ),
                        onYes: () {
                          context
                              .read<EmployeesBloc>()
                              .add(DeleteEmployeeEvent(employee));
                          context.router.pop();
                        },
                        onNo: () => context.router.pop(),
                      );
                    },
                    icon: const Icon(Icons.delete)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
