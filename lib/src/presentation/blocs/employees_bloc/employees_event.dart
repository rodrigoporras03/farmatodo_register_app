part of 'employees_bloc.dart';

@immutable
abstract class EmployeesEvent {}

class HomeLoadAllEmployeesEvent extends EmployeesEvent {}

class CreateEmployeeEvent extends EmployeesEvent {
  final String name;
  final String middleName;
  final String surname;
  final String lastName;

  final CountryEnum country;
  final IdTypeEnum idType;
  final String idNumber;

  final DateTime admisionDate;
  final AreaEnum area;
  final StatusEnum status;

  CreateEmployeeEvent({
    required this.name,
    required this.middleName,
    required this.surname,
    required this.lastName,
    required this.country,
    required this.idType,
    required this.idNumber,
    required this.admisionDate,
    required this.area,
    required this.status,
  });
}

class UpdateEmployeeEvent extends EmployeesEvent {
  final Employee employeeToUpdate;

  UpdateEmployeeEvent({required this.employeeToUpdate});
}

class DeleteEmployeeEvent extends EmployeesEvent {
  final Employee employeeToDelete;

  DeleteEmployeeEvent(this.employeeToDelete);
}
