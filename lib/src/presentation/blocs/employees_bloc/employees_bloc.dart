import 'package:bloc/bloc.dart';
import 'package:farmatodo_register_app/src/config/extensions/string.dart';
import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/domain/repositories/database_repository.dart';
import 'package:meta/meta.dart';

part 'employees_event.dart';
part 'employees_state.dart';

class EmployeesBloc extends Bloc<EmployeesEvent, EmployeesState> {
  final DatabaseRepository _databaseRepository;

  EmployeesBloc(this._databaseRepository) : super(EmployeesInitial()) {
    on<HomeLoadAllEmployeesEvent>(_loadAllEmployeesEventToState);
    on<CreateEmployeeEvent>(_createNewEmployeeEventToState);
    on<UpdateEmployeeEvent>(_updateEmployeeEventToState);
    on<DeleteEmployeeEvent>(_deleteEmployeeEvent);
  }

  _loadAllEmployeesEventToState(
    HomeLoadAllEmployeesEvent event,
    Emitter emit,
  ) async {
    emit(await _getAllEmployees());
  }

  _createNewEmployeeEventToState(
    CreateEmployeeEvent event,
    Emitter<EmployeesState> emit,
  ) async {
    //Verifiy type id and number id duplication

    if (state.allEmployees.any((employee) =>
        employee.idType == event.idType &&
        employee.idNumber == event.idNumber)) {
      emit(
        NewOrEditEmployeeErrorState(
          state.allEmployees,
          errorMessage:
              'Este tipo y numero de identificación ya está registrado',
        ),
      );
      return;
    }

    //Verify email duplication
    final finalEmail = _getFinalEmail(
      state.allEmployees,
      event.name,
      event.surname,
      event.country.getDomain(),
    );

    final newEmployee = Employee(
      id: null,
      name: event.name.trim(),
      middleName: event.middleName.trim(),
      surname: event.surname.trim(),
      lastName: event.lastName.trim(),
      country: event.country,
      idType: event.idType,
      idNumber: event.idNumber,
      email: finalEmail,
      admisionDate: event.admisionDate,
      area: event.area,
      status: event.status,
      registrationDate: DateTime.now(),
      lastUpdateDate: DateTime.now(),
    );

    await _databaseRepository.createEmployee(newEmployee);

    emit(EmployeCreatedState());

    emit(await _getAllEmployees());
  }

  _updateEmployeeEventToState(
    UpdateEmployeeEvent event,
    Emitter<EmployeesState> emit,
  ) async {
    //Verifiy type id and number id duplication

    if (state.allEmployees.any(
      (employee) =>
          employee.idType == event.employeeToUpdate.idType &&
          employee.idNumber == event.employeeToUpdate.idNumber &&
          employee.id != event.employeeToUpdate.id,
    )) {
      emit(
        NewOrEditEmployeeErrorState(
          state.allEmployees,
          errorMessage:
              'Este tipo y numero de identificación ya está registrado',
        ),
      );
      return;
    }

    //Verify email duplication

    final allEmployeesWithoutMe = state.allEmployees;
    allEmployeesWithoutMe.removeWhere((e) => e.id == event.employeeToUpdate.id);

    final finalEmail = _getFinalEmail(
      allEmployeesWithoutMe,
      event.employeeToUpdate.name,
      event.employeeToUpdate.surname,
      event.employeeToUpdate.country.getDomain(),
    );

    final newEmployee = event.employeeToUpdate.copyWith(
      email: finalEmail,
      lastUpdateDate: DateTime.now(),
    );

    await _databaseRepository.updateEmployee(newEmployee);

    emit(EmployeeEditedState());

    emit(await _getAllEmployees());
  }

  _deleteEmployeeEvent(
    DeleteEmployeeEvent event,
    Emitter emit,
  ) async {
    await _databaseRepository.deleteEmployee(event.employeeToDelete);

    emit(EmployeDeletedState());
    emit(await _getAllEmployees());
  }

  Future<EmployeesState> _getAllEmployees() async {
    final allEmployees = await _databaseRepository.getAllEmployees();

    return EmployeesLoadedState(allEmployees);
  }

  String _getFinalEmail(
    List<Employee> allEmployees,
    String name,
    String surname,
    String domain,
  ) {
    String email =
        '${name.trim()}.${surname.getWithoutBlank()}@${domain.trim()}';

    final allIdsWithMatchEmail = allEmployees
        .where((employee) => employee.email.contains(email.split('@').first))
        .map((employee) {
      RegExp reg = RegExp(r'(?<=\.)[^.@]+(?=@)');

      final result = reg.firstMatch(employee.email);
      int index = 0;
      if (result != null) {
        try {
          index = int.parse(result.group(0)!);
        } catch (e) {
          //
        }
      }

      return index;
    });

    int lastId = allIdsWithMatchEmail.isNotEmpty
        ? allIdsWithMatchEmail.reduce((a, b) => a > b ? a : b)
        : 0;

    if (allIdsWithMatchEmail.isNotEmpty) {
      email =
          '${name.trim()}.${surname.getWithoutBlank()}.${(lastId + 1)}@${domain.trim()}';
    }

    return email;
  }
}
