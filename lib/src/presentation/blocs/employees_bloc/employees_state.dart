part of 'employees_bloc.dart';

@immutable
abstract class EmployeesState {
  final List<Employee> allEmployees;

  const EmployeesState([this.allEmployees = const []]);
}

class EmployeesInitial extends EmployeesState {}

class EmployeesLoadedState extends EmployeesState {
  const EmployeesLoadedState(List<Employee> employees) : super(employees);
}

class NewOrEditEmployeeErrorState extends EmployeesState {
  final String errorMessage;

  const NewOrEditEmployeeErrorState(super.allEmployees,
      {required this.errorMessage});
}

class EmployeDeletedState extends EmployeesState {}

class EmployeCreatedState extends EmployeesState {}

class EmployeeEditedState extends EmployeesState {}
