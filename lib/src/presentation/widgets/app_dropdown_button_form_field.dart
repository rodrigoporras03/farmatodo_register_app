import 'package:flutter/material.dart';

class AppDropdownButtonFormField<T> extends StatelessWidget {
  const AppDropdownButtonFormField({
    super.key,
    required this.items,
    this.onChanged,
    this.hint,
    this.value,
  });

  final List<T> items;
  final Function(T?)? onChanged;
  final String? hint;
  final T? value;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<T>(
      hint: Text(hint ?? ''),
      value: value,
      validator: (value) =>
          value == null ? 'Por favor ingrese este campo' : null,
      items: items
          .map(
            (item) => DropdownMenuItem<T>(
              value: item,
              child: Text(item.toString()),
            ),
          )
          .toList(),
      onChanged: onChanged,
      decoration: const InputDecoration(
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(width: 4.0),
        ),
      ),
    );
  }
}
