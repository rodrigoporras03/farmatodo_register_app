import 'package:farmatodo_register_app/src/utils/formatters/uppercase_text_formatter.dart';
import 'package:flutter/material.dart';

class AppTextFormField extends StatelessWidget {
  final String? hintText;
  final bool enabled;
  final String? Function(String?)? validator;
  final TextEditingController? controller;
  final bool automaticUpperCase;
  final int? maxLength;
  final void Function(String)? onChanged;

  const AppTextFormField({
    super.key,
    this.hintText,
    this.enabled = true,
    this.validator,
    this.automaticUpperCase = false,
    this.maxLength,
    this.controller,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      inputFormatters: [
        if (automaticUpperCase) UpperCaseTextFormatter(),
      ],
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLength: maxLength,
      onChanged: onChanged,
      decoration: InputDecoration(
        filled: true,
        hintText: hintText,
        enabled: enabled,
        border: const OutlineInputBorder(
          borderSide: BorderSide(width: 4.0),
        ),
      ),
      validator: validator,
    );
  }
}
