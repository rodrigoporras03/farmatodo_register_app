import 'package:auto_route/auto_route.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/presentation/views/home/home_view.dart';
import 'package:farmatodo_register_app/src/presentation/views/new_or_edit_employee/new_or_edit_employee_view.dart';
import 'package:flutter/material.dart';

part 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'View,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: HomeRoute.page, path: '/'),
        AutoRoute(page: NewOrEditEmployeeRoute.page),
      ];
}

final appRouter = AppRouter();
