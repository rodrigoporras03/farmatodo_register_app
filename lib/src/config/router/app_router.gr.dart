// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: HomeView(key: args.key),
      );
    },
    NewOrEditEmployeeRoute.name: (routeData) {
      final args = routeData.argsAs<NewOrEditEmployeeRouteArgs>(
          orElse: () => const NewOrEditEmployeeRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: NewOrEditEmployeeView(
          key: args.key,
          employeeForEdit: args.employeeForEdit,
        ),
      );
    },
  };
}

/// generated route for
/// [HomeView]
class HomeRoute extends PageRouteInfo<HomeRouteArgs> {
  HomeRoute({
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          HomeRoute.name,
          args: HomeRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<HomeRouteArgs> page = PageInfo<HomeRouteArgs>(name);
}

class HomeRouteArgs {
  const HomeRouteArgs({this.key});

  final Key? key;

  @override
  String toString() {
    return 'HomeRouteArgs{key: $key}';
  }
}

/// generated route for
/// [NewOrEditEmployeeView]
class NewOrEditEmployeeRoute extends PageRouteInfo<NewOrEditEmployeeRouteArgs> {
  NewOrEditEmployeeRoute({
    Key? key,
    Employee? employeeForEdit,
    List<PageRouteInfo>? children,
  }) : super(
          NewOrEditEmployeeRoute.name,
          args: NewOrEditEmployeeRouteArgs(
            key: key,
            employeeForEdit: employeeForEdit,
          ),
          initialChildren: children,
        );

  static const String name = 'NewOrEditEmployeeRoute';

  static const PageInfo<NewOrEditEmployeeRouteArgs> page =
      PageInfo<NewOrEditEmployeeRouteArgs>(name);
}

class NewOrEditEmployeeRouteArgs {
  const NewOrEditEmployeeRouteArgs({
    this.key,
    this.employeeForEdit,
  });

  final Key? key;

  final Employee? employeeForEdit;

  @override
  String toString() {
    return 'NewOrEditEmployeeRouteArgs{key: $key, employeeForEdit: $employeeForEdit}';
  }
}
