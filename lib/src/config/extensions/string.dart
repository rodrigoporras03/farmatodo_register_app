extension StringExt on String {
  getWithoutBlank() => replaceAll(RegExp(r'\s+'), '');
}
