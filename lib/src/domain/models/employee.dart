// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:floor/floor.dart';

import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:farmatodo_register_app/src/utils/constants/strings.dart';

@Entity(tableName: employeesTable)
class Employee {
  @PrimaryKey(autoGenerate: true)
  final int? id;

  final String name;
  final String middleName;
  final String surname;
  final String lastName;

  final CountryEnum country;
  final IdTypeEnum idType;
  final String idNumber;
  final String email;

  final DateTime admisionDate;
  final AreaEnum area;
  final StatusEnum status;
  final DateTime registrationDate;
  final DateTime lastUpdateDate;

  Employee({
    required this.id,
    required this.name,
    required this.middleName,
    required this.surname,
    required this.lastName,
    required this.country,
    required this.idType,
    required this.idNumber,
    required this.email,
    required this.admisionDate,
    required this.area,
    required this.status,
    required this.registrationDate,
    required this.lastUpdateDate,
  });

  String getFullName() => '$name $middleName $surname $lastName';

  Employee copyWith({
    int? id,
    String? name,
    String? middleName,
    String? surname,
    String? lastName,
    CountryEnum? country,
    IdTypeEnum? idType,
    String? idNumber,
    String? email,
    DateTime? admisionDate,
    AreaEnum? area,
    StatusEnum? status,
    DateTime? registrationDate,
    DateTime? lastUpdateDate,
  }) {
    return Employee(
      id: id ?? this.id,
      name: name ?? this.name,
      middleName: middleName ?? this.middleName,
      surname: surname ?? this.surname,
      lastName: lastName ?? this.lastName,
      country: country ?? this.country,
      idType: idType ?? this.idType,
      idNumber: idNumber ?? this.idNumber,
      email: email ?? this.email,
      admisionDate: admisionDate ?? this.admisionDate,
      area: area ?? this.area,
      status: status ?? this.status,
      registrationDate: registrationDate ?? this.registrationDate,
      lastUpdateDate: lastUpdateDate ?? this.lastUpdateDate,
    );
  }
}
