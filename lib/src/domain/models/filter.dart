import 'package:farmatodo_register_app/src/domain/enums/filter_enum.dart';

class Filter {
  final FilterEnum filterEnum;
  final String query;

  Filter({
    required this.filterEnum,
    required this.query,
  });
}
