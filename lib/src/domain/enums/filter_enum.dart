enum FilterEnum {
  name('Por nombre'),
  middleName('Por segundo nombre'),
  surname('Por primer apellido'),
  lastName('Por segundo apellido'),
  idType('Por tipo de ID'),
  idNumber('Por numero de ID'),
  country('Por país'),
  email('Por correo electronico'),
  status('Por estado');

  const FilterEnum(this.tr);
  final String tr;

  @override
  toString() {
    return tr;
  }
}
