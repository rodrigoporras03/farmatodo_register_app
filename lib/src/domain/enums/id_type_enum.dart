enum IdTypeEnum {
  cc('Cedula De Ciudadania'),
  ce('Cedula De Extrajeria'),
  p('Pasaporte'),
  pe('Permiso Especial');

  const IdTypeEnum(this.tr);
  final String tr;

  @override
  toString() {
    return tr;
  }
}
