enum AreaEnum {
  admin('Administración'),
  financial('Financiera'),
  purchasing('Compras'),
  infrastructure('Infraestructura'),
  operations('Operación'),
  humanTalent('Talento Humano'),
  variusServices('Servicios Varios');

  const AreaEnum(this.tr);
  final String tr;
  @override
  toString() {
    return tr;
  }
}
