enum StatusEnum {
  active('Active');

  const StatusEnum(this.tr);
  final String tr;
   @override
  toString() {
    return tr;
  }
}
