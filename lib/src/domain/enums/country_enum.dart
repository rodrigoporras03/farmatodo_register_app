enum CountryEnum {
  colombia('Colombia'),
  unitedStates('Estados Unidos');

  const CountryEnum(this.tr);

  final String tr;

  @override
  toString() {
    return tr;
  }

  getDomain() {
    switch (this) {
      case colombia:
        return 'armirene.com.co';
      case unitedStates:
        return 'armirene.com.us';
    }
  }
}
