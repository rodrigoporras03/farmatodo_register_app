import 'package:farmatodo_register_app/src/domain/models/employee.dart';

abstract class DatabaseRepository {
  Future<List<Employee>> getAllEmployees();

  Future<void> createEmployee(Employee employee);

  Future<void> deleteEmployee(Employee employee);

  Future<void> updateEmployee(Employee employee);
}
