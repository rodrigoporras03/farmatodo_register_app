import 'dart:async';

import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/data/datasources/local/dao/employees_dao.dart';

import 'converters/converters.dart';

part 'app_database.g.dart';

@TypeConverters([
  DateTimeConverter,
  AreaEnumConverter,
  CountryEnumConverter,
  IdTypeEnumConverter,
  StatusEnumConverter,
])
@Database(version: 1, entities: [Employee])
abstract class AppDatabase extends FloorDatabase {
  EmployessDao get employessDao;
}
