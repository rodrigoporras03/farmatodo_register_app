import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:floor/floor.dart';

class CountryEnumConverter extends TypeConverter<CountryEnum, String> {
  @override
  CountryEnum decode(String databaseValue) {
    return CountryEnum.values.byName(databaseValue);
  }

  @override
  String encode(CountryEnum value) {
    return value.name;
  }
}
