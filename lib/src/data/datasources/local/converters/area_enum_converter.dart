import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:floor/floor.dart';

class AreaEnumConverter extends TypeConverter<AreaEnum, String> {
  @override
  AreaEnum decode(String databaseValue) {
    return AreaEnum.values.byName(databaseValue);
  }

  @override
  String encode(AreaEnum value) {
    return value.name;
  }
}
