import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:floor/floor.dart';

class IdTypeEnumConverter extends TypeConverter<IdTypeEnum, String> {
  @override
  IdTypeEnum decode(String databaseValue) {
    return IdTypeEnum.values.byName(databaseValue);
  }

  @override
  String encode(IdTypeEnum value) {
    return value.name;
  }
}
