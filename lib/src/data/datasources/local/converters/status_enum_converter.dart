import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:floor/floor.dart';

class StatusEnumConverter extends TypeConverter<StatusEnum, String> {
  @override
  StatusEnum decode(String databaseValue) {
    return StatusEnum.values.byName(databaseValue);
  }

  @override
  String encode(StatusEnum value) {
    return value.name;
  }
}
