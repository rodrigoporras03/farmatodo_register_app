import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/utils/constants/strings.dart';
import 'package:floor/floor.dart';

@dao
abstract class EmployessDao {
  @Query('SELECT * FROM $employeesTable')
  Future<List<Employee>> getAllEmployees();

  @Insert(onConflict: OnConflictStrategy.fail)
  Future<void> createEmployee(Employee employee);

  @delete
  Future<void> deleteEmployee(Employee employee);

  @Update(onConflict: OnConflictStrategy.replace)
  Future<void> updateEmployee(Employee employee);
}
