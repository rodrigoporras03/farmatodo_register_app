// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  EmployessDao? _employessDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `employees_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `middleName` TEXT NOT NULL, `surname` TEXT NOT NULL, `lastName` TEXT NOT NULL, `country` TEXT NOT NULL, `idType` TEXT NOT NULL, `idNumber` TEXT NOT NULL, `email` TEXT NOT NULL, `admisionDate` INTEGER NOT NULL, `area` TEXT NOT NULL, `status` TEXT NOT NULL, `registrationDate` INTEGER NOT NULL, `lastUpdateDate` INTEGER NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  EmployessDao get employessDao {
    return _employessDaoInstance ??= _$EmployessDao(database, changeListener);
  }
}

class _$EmployessDao extends EmployessDao {
  _$EmployessDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _employeeInsertionAdapter = InsertionAdapter(
            database,
            'employees_table',
            (Employee item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'middleName': item.middleName,
                  'surname': item.surname,
                  'lastName': item.lastName,
                  'country': _countryEnumConverter.encode(item.country),
                  'idType': _idTypeEnumConverter.encode(item.idType),
                  'idNumber': item.idNumber,
                  'email': item.email,
                  'admisionDate': _dateTimeConverter.encode(item.admisionDate),
                  'area': _areaEnumConverter.encode(item.area),
                  'status': _statusEnumConverter.encode(item.status),
                  'registrationDate':
                      _dateTimeConverter.encode(item.registrationDate),
                  'lastUpdateDate':
                      _dateTimeConverter.encode(item.lastUpdateDate)
                }),
        _employeeUpdateAdapter = UpdateAdapter(
            database,
            'employees_table',
            ['id'],
            (Employee item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'middleName': item.middleName,
                  'surname': item.surname,
                  'lastName': item.lastName,
                  'country': _countryEnumConverter.encode(item.country),
                  'idType': _idTypeEnumConverter.encode(item.idType),
                  'idNumber': item.idNumber,
                  'email': item.email,
                  'admisionDate': _dateTimeConverter.encode(item.admisionDate),
                  'area': _areaEnumConverter.encode(item.area),
                  'status': _statusEnumConverter.encode(item.status),
                  'registrationDate':
                      _dateTimeConverter.encode(item.registrationDate),
                  'lastUpdateDate':
                      _dateTimeConverter.encode(item.lastUpdateDate)
                }),
        _employeeDeletionAdapter = DeletionAdapter(
            database,
            'employees_table',
            ['id'],
            (Employee item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'middleName': item.middleName,
                  'surname': item.surname,
                  'lastName': item.lastName,
                  'country': _countryEnumConverter.encode(item.country),
                  'idType': _idTypeEnumConverter.encode(item.idType),
                  'idNumber': item.idNumber,
                  'email': item.email,
                  'admisionDate': _dateTimeConverter.encode(item.admisionDate),
                  'area': _areaEnumConverter.encode(item.area),
                  'status': _statusEnumConverter.encode(item.status),
                  'registrationDate':
                      _dateTimeConverter.encode(item.registrationDate),
                  'lastUpdateDate':
                      _dateTimeConverter.encode(item.lastUpdateDate)
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Employee> _employeeInsertionAdapter;

  final UpdateAdapter<Employee> _employeeUpdateAdapter;

  final DeletionAdapter<Employee> _employeeDeletionAdapter;

  @override
  Future<List<Employee>> getAllEmployees() async {
    return _queryAdapter.queryList('SELECT * FROM employees_table',
        mapper: (Map<String, Object?> row) => Employee(
            id: row['id'] as int?,
            name: row['name'] as String,
            middleName: row['middleName'] as String,
            surname: row['surname'] as String,
            lastName: row['lastName'] as String,
            country: _countryEnumConverter.decode(row['country'] as String),
            idType: _idTypeEnumConverter.decode(row['idType'] as String),
            idNumber: row['idNumber'] as String,
            email: row['email'] as String,
            admisionDate: _dateTimeConverter.decode(row['admisionDate'] as int),
            area: _areaEnumConverter.decode(row['area'] as String),
            status: _statusEnumConverter.decode(row['status'] as String),
            registrationDate:
                _dateTimeConverter.decode(row['registrationDate'] as int),
            lastUpdateDate:
                _dateTimeConverter.decode(row['lastUpdateDate'] as int)));
  }

  @override
  Future<void> createEmployee(Employee employee) async {
    await _employeeInsertionAdapter.insert(employee, OnConflictStrategy.fail);
  }

  @override
  Future<void> updateEmployee(Employee employee) async {
    await _employeeUpdateAdapter.update(employee, OnConflictStrategy.replace);
  }

  @override
  Future<void> deleteEmployee(Employee employee) async {
    await _employeeDeletionAdapter.delete(employee);
  }
}

// ignore_for_file: unused_element
final _dateTimeConverter = DateTimeConverter();
final _areaEnumConverter = AreaEnumConverter();
final _countryEnumConverter = CountryEnumConverter();
final _idTypeEnumConverter = IdTypeEnumConverter();
final _statusEnumConverter = StatusEnumConverter();
