import 'package:farmatodo_register_app/src/data/datasources/local/app_database.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/domain/repositories/database_repository.dart';

class DatabaseRepositoryImpl implements DatabaseRepository {
  final AppDatabase _appDatabase;

  DatabaseRepositoryImpl(this._appDatabase);

  @override
  Future<List<Employee>> getAllEmployees() {
    return _appDatabase.employessDao.getAllEmployees();
  }

  @override
  Future<void> createEmployee(Employee employee) {
    return _appDatabase.employessDao.createEmployee(employee);
  }

  @override
  Future<void> deleteEmployee(Employee employee) {
    return _appDatabase.employessDao.deleteEmployee(employee);
  }

  @override
  Future<void> updateEmployee(Employee employee) {
    return _appDatabase.employessDao.updateEmployee(employee);
  }
}
