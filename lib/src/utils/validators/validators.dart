class Validators {
  static RegExp regOnlyAZAndNoAccentWithSpace = RegExp(r'^[A-Z\s]*$');
  static RegExp regidNumber = RegExp(r'^[a-zA-Z0-9-]+$');
  static RegExp regOnlyAZAndNoAccent = RegExp(r'^[A-Z]*$');

  static String? validateSurnnames(String? name) {
    if (name == null || name.isEmpty) {
      return 'Por favor ingrese este campo';
    }

    if (!regOnlyAZAndNoAccentWithSpace.hasMatch(name)) {
      return 'Solo se permiten caracteres de la A a la Z, sin acentos y sin Ñ';
    }
    return null;
  }



  static String? validateNames(String? name) {
    if (!regOnlyAZAndNoAccent.hasMatch(name ?? '')) {
      return 'Solo se permiten caracteres de la A a la Z, sin acentos, sin ñ y sin espacios';
    }
    return null;
  }

  static String? validateIdNumber(String? idNumber) {
    if (idNumber == null || idNumber.isEmpty) {
      return 'Por favor ingrese este campo';
    }
    if (!regidNumber.hasMatch(idNumber)) {
      return 'Solo se permiten caracteres a-z, A-Z,0-9, caracter especial - y sin espacios';
    }
    return null;
  }
}
