import 'package:farmatodo_register_app/src/locator.dart';
import 'package:logger/logger.dart';

class Log {
  static Logger get to => locator.get<Logger>();
}
