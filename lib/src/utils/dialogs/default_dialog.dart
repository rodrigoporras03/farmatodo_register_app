import 'package:auto_route/auto_route.dart';
import 'package:farmatodo_register_app/src/utils/constants/app_default_insets.dart';
import 'package:flutter/material.dart';

Future showDefaultDialog({
  required BuildContext context,
  required Widget content,
  String? title,
  Function? onClose,
  bool barrierDismissible = true,
  Color? backgroundColor,
  bool showOkbtn = true,
  String? labelYes,
  String? labelNo,
  void Function()? onYes,
  void Function()? onNo,
}) {
  return showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext context) => StatefulBuilder(
      builder: (context, snapshot) {
        return AlertDialog(
          backgroundColor: backgroundColor,
          insetPadding: const EdgeInsets.symmetric(
            horizontal: AppDefaultInsets.s40,
            vertical: AppDefaultInsets.s24,
          ),
          title: title == null
              ? null
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      title,
                      style: const TextStyle(fontSize: 16.0),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
          content: IntrinsicHeight(
            child: SingleChildScrollView(child: content),
          ),
          actions: [
            if (showOkbtn && labelNo == null && labelYes == null)
              TextButton(
                onPressed: () async {
                  if (onClose != null) {
                    onClose.call();
                  }
                  await Future.delayed(Duration.zero);
                  context.router.pop();
                },
                child: const Text('OK'),
              ),
            if (labelYes != null)
              TextButton(
                onPressed: onYes,
                child: Text(labelYes),
              ),
            if (labelNo != null)
              TextButton(
                onPressed: onNo,
                child: Text(labelNo),
              )
          ],
        );
      },
    ),
  );
}
