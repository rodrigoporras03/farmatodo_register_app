import 'package:farmatodo_register_app/src/data/datasources/local/app_database.dart';
import 'package:farmatodo_register_app/src/data/repositories/database_repository_impl.dart';
import 'package:farmatodo_register_app/src/domain/repositories/database_repository.dart';
import 'package:farmatodo_register_app/src/utils/constants/strings.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';

final locator = GetIt.instance;

Future<void> initializeDependencies() async {
  final db = await $FloorAppDatabase.databaseBuilder(databaseName).build();
  locator.registerSingleton<AppDatabase>(db);

  locator.registerSingleton<DatabaseRepository>(
    DatabaseRepositoryImpl(locator<AppDatabase>()),
  );

  locator.registerSingleton<Logger>(Logger());
}
