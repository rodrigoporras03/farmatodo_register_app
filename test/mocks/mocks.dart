import 'package:auto_route/auto_route.dart';
import 'package:farmatodo_register_app/src/data/repositories/database_repository_impl.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks(
  [
    DatabaseRepositoryImpl,
    AutoRouter,
  ],
)
void main() {}
