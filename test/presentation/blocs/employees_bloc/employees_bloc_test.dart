import 'package:bloc_test/bloc_test.dart';
import 'package:farmatodo_register_app/src/domain/enums/enums.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/presentation/blocs/employees_bloc/employees_bloc.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../mocks/mocks.mocks.dart';

void main() {
  late EmployeesBloc employeesBloc;
  late MockDatabaseRepositoryImpl mockDatabaseRepository;

  List<Employee> allEmployees = [
    Employee(
      id: 1,
      name: 'Juan',
      middleName: 'Guillermo',
      surname: 'Ortiz',
      lastName: 'Gonzales',
      country: CountryEnum.colombia,
      idType: IdTypeEnum.cc,
      idNumber: '123',
      email: 'juan.ortiz@armirene.com.co',
      admisionDate: DateTime.now(),
      area: AreaEnum.admin,
      status: StatusEnum.active,
      registrationDate: DateTime.now(),
      lastUpdateDate: DateTime.now(),
    ),
    Employee(
      id: 2,
      name: 'Nicolas',
      middleName: 'Manuel',
      surname: 'Guzman',
      lastName: 'Gonzales',
      country: CountryEnum.unitedStates,
      idType: IdTypeEnum.cc,
      idNumber: '123',
      email: 'nicolas.guzman@armirene.com.us',
      admisionDate: DateTime.now(),
      area: AreaEnum.admin,
      status: StatusEnum.active,
      registrationDate: DateTime.now(),
      lastUpdateDate: DateTime.now(),
    ),
  ];

  setUp(() {
    mockDatabaseRepository = MockDatabaseRepositoryImpl();
    employeesBloc = EmployeesBloc(mockDatabaseRepository);
  });

  tearDown(() {
    employeesBloc.close();
  });

  group('EmployeesBloc', () {
    test('initial state should be EmployeesInitial', () {
      expect(employeesBloc.state, isA<EmployeesInitial>());
    });

    test('emits EmployeesLoadedState after HomeLoadAllEmployeesEvent',
        () async {
      when(mockDatabaseRepository.getAllEmployees())
          .thenAnswer((_) async => allEmployees);

      employeesBloc.add(HomeLoadAllEmployeesEvent());
      await Future.delayed(Duration.zero);
      expect(employeesBloc.state, isA<EmployeesLoadedState>());
    });

    blocTest<EmployeesBloc, EmployeesState>(
      'emits NewOrEditEmployeeErrorState if idType and idNumber are duplicated',
      build: () => employeesBloc,
      act: (bloc) {
        when(mockDatabaseRepository.getAllEmployees())
            .thenAnswer((_) async => allEmployees);
        bloc.add(HomeLoadAllEmployeesEvent());

        bloc.add(CreateEmployeeEvent(
          idType: IdTypeEnum.cc,
          idNumber: '123',
          name: 'Nicolas',
          middleName: 'Manuel',
          surname: 'Guzman',
          lastName: 'Gonzales',
          country: CountryEnum.unitedStates,
          admisionDate: DateTime.now(),
          area: AreaEnum.admin,
          status: StatusEnum.active,
        ));
      },
      expect: () => [
        isA<EmployeesLoadedState>(),
        isA<NewOrEditEmployeeErrorState>(),
      ],
    );

    blocTest<EmployeesBloc, EmployeesState>(
      'emits EmployeDeletedState and EmployeesLoadedState after DeleteEmployeeEvent',
      build: () => employeesBloc,
      act: (bloc) async {
        when(mockDatabaseRepository.getAllEmployees())
            .thenAnswer((_) async => allEmployees);

        when(mockDatabaseRepository.deleteEmployee(allEmployees.first))
            .thenAnswer((_) async => true);

        bloc.add(DeleteEmployeeEvent(allEmployees.first));

        expectLater(
          bloc.stream,
          emitsInOrder([
            EmployeDeletedState(),
            EmployeesLoadedState(allEmployees),
          ]),
        );
      },
    );
  });
}
