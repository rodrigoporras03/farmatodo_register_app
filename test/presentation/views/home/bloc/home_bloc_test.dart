import 'package:bloc_test/bloc_test.dart';
import 'package:farmatodo_register_app/src/domain/enums/filter_enum.dart';
import 'package:farmatodo_register_app/src/domain/models/employee.dart';
import 'package:farmatodo_register_app/src/domain/models/filter.dart';
import 'package:farmatodo_register_app/src/presentation/views/home/bloc/home_bloc.dart';
import 'package:test/test.dart';

void main() {
  late HomeBloc homeBloc;

  List<Employee> allEmployees = [];

  setUp(() {
    homeBloc = HomeBloc();
  });

  tearDown(() {
    homeBloc.close();
  });

  group('HomeBloc', () {
    test('initial state should be HomeInitial', () {
      expect(homeBloc.state, isA<HomeInitial>());
    });

    blocTest<HomeBloc, HomeState>(
      'emits HomeShowFilteredList after HomeAddFilterEvent',
      build: () => homeBloc,
      act: (bloc) {
        bloc.add(
          HomeAddFilterEvent(
            allEmployees,
            Filter(filterEnum: FilterEnum.name, query: 'John'),
          ),
        );
      },
      expect: () => [isA<HomeShowFilteredList>()],
    );

    blocTest<HomeBloc, HomeState>(
      'emits HomeInitial after HomeRemoveAllFiltersEvent',
      build: () => homeBloc,
      act: (bloc) {
        bloc.add(HomeRemoveAllFiltersEvent());
      },
      expect: () => [isA<HomeInitial>()],
    );
  });
}
