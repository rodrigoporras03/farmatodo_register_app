import 'package:farmatodo_register_app/src/domain/enums/filter_enum.dart';
import 'package:farmatodo_register_app/src/presentation/views/home/widgets/add_filter.dart';
import 'package:farmatodo_register_app/src/presentation/widgets/app_dropdown_button_form_field.dart';
import 'package:farmatodo_register_app/src/presentation/widgets/app_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  getMateAPP() => const MaterialApp(
        home: Material(child: AddFilter()),
      );

  group('AddFilter widget', () {
    testWidgets('renders correctly', (WidgetTester tester) async {
      await tester.pumpWidget(getMateAPP());

      expect(
          find.byType(AppDropdownButtonFormField<FilterEnum>), findsOneWidget);
      expect(
          find.byWidgetPredicate(
              (Widget widget) => widget is SizedBox && widget.height == 16),
          findsOneWidget);
      expect(find.byType(TextButton), findsOneWidget);
    });

    testWidgets('selecting a filter shows the corresponding filter field',
        (WidgetTester tester) async {
      await tester.pumpWidget(getMateAPP());

      final dropdownButtonFormField =
          find.byType(AppDropdownButtonFormField<FilterEnum>).first;

      // Open dropdown
      await tester.tap(dropdownButtonFormField);
      await tester.pumpAndSettle();

      final dropdownItem = find.byType(DropdownMenuItem<FilterEnum>).first;

      // Select first item (FilterEnum.name)
      await tester.tap(dropdownItem);
      await tester.pumpAndSettle();

      // Check that the corresponding filter field (AppTextFormField) is shown
      expect(find.byType(AppTextFormField), findsOneWidget);
    });
  });
}
