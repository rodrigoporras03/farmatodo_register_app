import 'package:bloc_test/bloc_test.dart';
import 'package:farmatodo_register_app/src/presentation/views/new_or_edit_employee/bloc/new_or_edit_employee_bloc.dart';
import 'package:test/test.dart';

void main() {
  late NewOrEditEmployeeBloc newOrEditEmployeeBloc;

  setUp(() {
    newOrEditEmployeeBloc = NewOrEditEmployeeBloc();
  });

  tearDown(() {
    newOrEditEmployeeBloc.close();
  });

  group('NewOrEditEmployeeBloc', () {
    test('initial state should be NewOrEditEmployeeInitial', () {
      expect(newOrEditEmployeeBloc.state, isA<NewOrEditEmployeeInitial>());
    });

    blocTest<NewOrEditEmployeeBloc, NewOrEditEmployeeState>(
      'emits UpdateRegistrationDateState after UpdateAdmisionDateFieldEvent',
      build: () => newOrEditEmployeeBloc,
      act: (bloc) {
        bloc.add(UpdateAdmisionDateFieldEvent(DateTime.now()));
      },
      expect: () => [isA<UpdateRegistrationDateState>()],
    );
  });
}
